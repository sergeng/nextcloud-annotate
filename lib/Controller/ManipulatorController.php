<?php
declare(strict_types=1);
namespace OCA\PDFAnnotate\Controller;

use OCP\AppFramework\Controller;
use OCP\AppFramework\Http;
use OCP\AppFramework\Http\ContentSecurityPolicy;
use OCP\AppFramework\Http\DataDisplayResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Http\RedirectResponse;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\Constants;
use OCP\Files\Folder;
use OCP\Files\IRootFolder;
use OCP\Files\NotFoundException;
use OCP\IDBConnection;
use OCP\IRequest;
use OCP\ISession;
use OCP\IUser;
use OCP\IUserSession;
use OCP\Share\Exceptions\ShareNotFound;
use OCP\Share\IManager;

class ManipulatorController extends Controller {

	private $db;
	private $userId;
	private $root;
	private $userSession;

	/**
	 * @param string $AppName
	 * @param IRequest $request
	 * @param $userId
	 * @param IRootFolder $root
	 */
	public function __construct(
			string $AppName,
			IRequest $request,
			$UserId,
			IRootFolder $root,
			IUserSession $userSession,
			IDBConnection $db
			) {
		parent::__construct($AppName, $request);
		$this->userId = $UserId;
		$this->userSession = $userSession;
		$this->root = $root;
		$this->db = $db;
	}

        /**
         * @NoAdminRequired
         * @NoCSRFRequired
         *
         * @return DataResponse
         */
	public function save(string $fileId, bool $build, array $pages){
		error_log("Project file: ".$fileId);

		$this->db->beginTransaction();

		try {
			$query = $this->db->getQueryBuilder();
			$query->delete('pdfmanipulator_pages')
				->where($query->expr()->eq('pdfm_fileid', $query->createNamedParameter($fileId)));
			$query->execute();
		} catch (Exception $e){
			$this->db->rollBack();
			return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
		}

		for ($i = 0; $i < count($pages); $i++){
			$page = explode("_", $pages[$i]);
			error_log("Page $i: ".$page[1]."/".$page[2]);

			try {
				$query = $this->db->getQueryBuilder();
				$query->insert('pdfmanipulator_pages')
					->values([
						'pdfm_fileid' => $query->createNamedParameter($fileId),
						'page_fileid' => $query->createNamedParameter($page[1]),
						'page' => $query->createNamedParameter($page[2]),
						'order' => $query->createNamedParameter($i)
					]);
				$query->execute();
			} catch (Exception $e){
				$this->db->rollBack();
				return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
			}
		}

		$this->db->commit();

		if ($build && !$this->buildPDF($fileId)) return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);

		return new DataResponse([], Http::STATUS_OK);
	}

	/**
	 * Suck the requested page out of a PDF and send it back to client as a PNG for preview.
	 *
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @return DataDisplayResponse
	 */
	public function preview(string $fileId, string $page){

		error_log("Preview requested: ".$fileId."/".$page);

		$file = $this->root->getUserFolder($this->userId)->getById($fileId)[0];
		if (!$file instanceof \OCP\Files\File) return new DataResponse([], Http::STATUS_NOT_FOUND);

		try {
			$content = $file->getContent();
		} catch(\OCP\Files\NotFoundException $e) {
			return new DataResponse([], Http::STATUS_NOT_FOUND);
		}

		$descriptorspec = array(
			0 => array("pipe", "r"),
			1 => array("pipe", "w"),
		);

		$process = proc_open("gs -q -dBATCH -dNOPAUSE -sDEVICE=png16m -dFirstPage=$page -dLastPage=$page -dAutoRotatePages=/None -sOutputFile=- -", $descriptorspec, $pipes);

		if (is_resource($process)) {
			fwrite($pipes[0], $content);
			fclose($pipes[0]);

			$preview = stream_get_contents($pipes[1]);
			fclose($pipes[1]);

			$return_value = proc_close($process);

			if ($return_value === 0)
				return new DataDisplayResponse($preview, Http::STATUS_OK, ['Content-Type' => 'image/png']);
		}
		return new DataResponse([], Http::STATUS_NOT_FOUND);
	}

	/**
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @return TemplateResponse
	 */
	public function manipulator(string $fileId) {
		$query = $this->db->getQueryBuilder();
		$query->select('*')
			->from('pdfmanipulator_pages', 'p')
			->leftJoin('p', 'filecache', 'f', $query->expr()->eq('p.page_fileid', 'f.fileid'))
			->where($query->expr()->eq('pdfm_fileid', $query->createNamedParameter($fileId)))
			->orderBy('p.order', 'ASC');
		$result = $query->execute();
		$pages = array();
		while ($row = $result->fetch()){
			error_log("writing a row");
			$pages[] = $row;
		}
		$result->closeCursor();
		$parameters = array();
		$parameters['pages'] = $pages;
		$parameters['fileId'] = $fileId;

		foreach($parameters['pages'] as $page) error_log("page: ".$page['order']);
		return new TemplateResponse($this->appName, 'manipulator', $parameters);
	}

	/**
	 * Add page to manipulation project.
	 *
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @param string $fileId
	 * @param string $page
	 * @return DataResponse
	 */
	public function addPage(string $fileId, string $page) {
		error_log("PDF Annotator adding page/fileid: $page/$fileId");

		$userid = $this->userSession->getUser()->getUID();

		// Verify that the file exists;
		$file = $this->root->getUserFolder($userid)->getById($fileId)[0];
		if (!$file instanceof \OCP\Files\File) return new DataResponse([], Http::STATUS_NOT_FOUND);

		// Find a pdfm file in the *current* directory
		$parent = $file->getParent();
		$listing = $parent->getDirectoryListing();
		$pdfmid = 0;
		foreach ($listing AS $node){
			if ($node->getType() === 'file' && $node->getExtension() === 'pdfm'){
				$pdfmid = $node->getId();
				break;
			}
		}
		if ($pdfmid == null || $pdfmid < 1)
			return new DataResponse([], Http::STATUS_NOT_FOUND);

		// If we are here, we have all the data we need to add the entry.

		$query = $this->db->getQueryBuilder();
		$query->select('*')
			->from('pdfmanipulator_pages')
			->where($query->expr()->eq('pdfm_fileid', $query->createNamedParameter($pdfmid)));
		$result = $query->execute();
		$order = 0;
		while ($row = $result->fetch()){
			$order = max($order, $row['order']);
		}
		$order++;

		$query = $this->db->getQueryBuilder();
		$query->insert('pdfmanipulator_pages')->values([
			'order' => $order,
			'pdfm_fileid' => $pdfmid,
			'page_fileid' => $fileId,
			'page' => $page,
		]);
		$query->execute();

		return new DataResponse([], Http::STATUS_OK);
	}

	/**
	 * Build PDF for manipulation project.
	 */
	private function buildPDF(string $fileId) {

		$tmp_path = sys_get_temp_dir() . "/nc_pdfanno-" . getmypid();
		if (!mkdir($tmp_path, 0700)) return false;

		$userid = $this->userSession->getUser()->getUID();

		// Collect the source PDF's...
		$query = $this->db->getQueryBuilder();
		$query->selectDistinct('page_fileid')
			->from('pdfmanipulator_pages')
			->where($query->expr()->eq('pdfm_fileid', $query->createNamedParameter($fileId)));
		$result = $query->execute();
		while ($row = $result->fetch()){
			$file = $this->root->getUserFolder($userid)->getById($row['page_fileid'])[0];
			if (!$file instanceof \OCP\Files\File) return false;

			try {
				$content = $file->getContent();
			} catch(\OCP\Files\NotFoundException $e) {
				return false;
			}

			if (file_put_contents("$tmp_path/".$row['page_fileid'].".pdf", $content) == false)
				return false;
		}

		// Collect all the pages as single-page PDF's...
		$query = $this->db->getQueryBuilder();
		$query->select('*')
			->from('pdfmanipulator_pages')
			->where($query->expr()->eq('pdfm_fileid', $query->createNamedParameter($fileId)));
		$result = $query->execute();
		while ($row = $result->fetch()){
			$page = $row['page'];
			$order = str_pad(strval($row['order']), 8, "0", STR_PAD_LEFT);
			$file = $row['page_fileid'];
			system("gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dFirstPage=$page -dLastPage=$page -dAutoRotatePages=/None -sOutputFile=$tmp_path/page_$order.pdf $tmp_path/$file.pdf");
		}

		// Put all the pages together.
		system("gs -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOutputFile=$tmp_path/assembled.pdf $tmp_path/page_*.pdf");

		// Add the new file to NextCloud.
		$content = file_get_contents("$tmp_path/assembled.pdf");
		try {
			$pdfm_file = $this->root->getUserFolder($userid)->getById($fileId)[0];
			$file = $pdfm_file->getParent()->newFile(str_replace(".pdfm", ".pdf", $pdfm_file->getName()), $content);
		} catch(\OCP\Files\NotFoundException $e) {
			return false;
		}
		if (!$file instanceof \OCP\Files\File) return false;

		// Clean up and return.
		system("rm -rf $tmp_path");
		return true;
	}

	/**
	 * Add page to manipulation project.
	 *
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 *
	 * @param string $fileId
	 * @param string $fileName
	 * @return DataResponse
	 */
	public function newProject(string $fileId, string $fileName) {
		error_log("PDF Annotator adding project directory/name: $fileId/$fileName");

		$userid = $this->userSession->getUser()->getUID();

		// Verify that the directory exists;
		$directory = $this->root->getUserFolder($userid)->getById($fileId)[0];
		if (!$directory instanceof \OCP\Files\Folder) return new DataResponse([], Http::STATUS_NOT_FOUND);

		error_log("Directory path: ".$directory->getPath());
		$listing = $directory->getDirectoryListing();
		foreach ($listing as $node){
			if ($node instanceof \OCP\Files\File && strcmp($node->getExtension(), "pdfm") == 0)
				return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
		}
		try {
			$file = $directory->newFile($fileName.".pdfm", "");
		} catch(Exception $e){
			error_log("Failed to create new file");
			return new DataResponse([], Http::STATUS_INTERNAL_SERVER_ERROR);
		}

		return new DataResponse([], Http::STATUS_OK);
	}
}
